package cz.vse.pavlu.model;

/**
 * Třída implementující příkaz pro oslovení osoby/nepřítele.
 * @author David Pavlů
 * @version LS 2020
 */
public class CommandTalk implements ICommand
{
    private static final String NAME = "oslov";
    
    private GamePlan plan;
    
    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandTalk(GamePlan plan) {
        this.plan = plan;
    }    
    
    /**
     * Metoda vypíše popis u dané osoby, nejdříve však zkontroluje správně zadaný parametr a zda oblast osobu obsahuje.
     * @param parameters parametry příkazu (očekává se pole s jedním prvkem)
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    public String process(String... parameters) {
        if (parameters.length == 0) {
            return "Nevím, koho mám oslovit, musíš zadat název osoby.";
        }
        
        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím oslovit více osob současně.";
        }
        
        String enemyName = parameters[0];
        Area area = plan.getCurrentArea();
         
        if (!area.containsEnemy(enemyName)) {
            return enemyName + " nelze oslovit.";
        }

        return area.getEnemy(enemyName).getDescription();
    }
    
    /**
     * Metoda vrací název příkazu.
     * @return název příkazu
     */
    public String getName() {
        return NAME;
    }
}
