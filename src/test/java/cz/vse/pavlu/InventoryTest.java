package cz.vse.pavlu;

import cz.vse.pavlu.model.Game;
import cz.vse.pavlu.model.Inventory;
import cz.vse.pavlu.model.Item;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;

/**
 * Testovací třída pro komplexní otestování inventáře.
 *
 * @author David Pavlů
 * @version LS 2020
 */
public class InventoryTest
{
    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testAddToInventory() {
        //kontrola mista v inventu, je v inventu, neni v inventu nevesel se
        Inventory inventory = new Inventory(game.getGamePlan());

        Item item1 = new Item("rum", "Láhev vyzrálého rumu.");
        Item item2 = new Item("chleb", "Starý tvrdý chléb.");
        Item item3 = new Item("vino", "Zkažené červené víno.");
        
        assertTrue(inventory.addToInventory(item1.getName(), item1));
        assertTrue(inventory.addToInventory(item2.getName(), item2));
        assertFalse(inventory.addToInventory(item3.getName(), item3));
        
        assertTrue(inventory.containsItem(item1.getName()));
        assertTrue(inventory.containsItem(item2.getName()));
        assertFalse(inventory.containsItem(item3.getName()));
    }
    
    @Test
    public void testRemoveItem() {
        //odebere z inventu, da na zem - vytvorit invent, neosahuje, vytvorit item, obsahuje, odebrat z inventu, neobsahuje v inventu, obasuje v lokaci
        Inventory inventory = new Inventory(game.getGamePlan());

        Item item1 = new Item("rum", "Láhev vyzrálého rumu.");
        
        inventory.addToInventory(item1.getName(), item1);
        
        assertTrue(inventory.containsItem(item1.getName()));
        assertFalse(game.getGamePlan().getCurrentArea().containsItem(item1.getName()));
        
        inventory.removeItem(item1.getName());
        
        assertFalse(inventory.containsItem(item1.getName()));
        assertTrue(game.getGamePlan().getCurrentArea().containsItem(item1.getName()));
    }
    
    @Test
    public void testRemoveItemTotally() {
        //odebere item z inventu i ze hry (arei?) - vytvorit invent, neosahuje, vytvorit item, obsahuje, odebrat uplne, neobsahuje
        Inventory inventory = new Inventory(game.getGamePlan());

        Item item1 = new Item("rum", "Láhev vyzrálého rumu.");
        Item item2 = new Item("chleb", "Starý tvrdý chléb.");
        
        inventory.addToInventory(item1.getName(), item1);
        
        assertTrue(inventory.containsItem(item1.getName()));
        assertFalse(inventory.containsItem(item2.getName()));
        
        inventory.removeItemTotally(item1.getName());
        
        assertFalse(inventory.containsItem(item1.getName()));
        assertFalse(inventory.containsItem(item2.getName()));
    }
    
    @Test
    public void testContainsItem() {
        //obsahuje predmet v inventu - vytvorit invent, neobsahuje, vytvorit item, pridat, obsahuje
        Inventory inventory = new Inventory(game.getGamePlan());

        Item item1 = new Item("rum", "Láhev vyzrálého rumu.");
        Item item2 = new Item("chleb", "Starý tvrdý chléb.");
        
        inventory.addToInventory(item1.getName(), item1);
        
        assertTrue(inventory.containsItem(item1.getName()));
        assertFalse(inventory.containsItem(item2.getName()));
    }
}
