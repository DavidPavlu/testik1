package cz.vse.pavlu.model;

/**
 * Třída implementující příkaz pro odebrání předmětu z inventáře.
 * @author David Pavlů
 * @version LS 2020
 */
public class CommandRemoveItem implements ICommand
{
    private static final String NAME = "poloz";
    private Inventory inventory;
    
    /**
     * Konstruktor třídy.
     * @param inventory
     */
    public CommandRemoveItem(Inventory inventory) {
        this.inventory = inventory;
    }
    
    /**
     * Metoda zavolá metodu třídy inventář a podle parametru odstraní daný předmět.
     * @param parameters parametry příkazu (očekává se pole s jedním prvkem)
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    @Override
    public String process(String... parameters) {
        if (parameters.length == 0) {
            return "Nevím, co mám položit, musíš zadat název předmětu.";
        }
        
        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím položit více předmětů současně.";
        }
        
        String itemName = parameters[0];
        
        return inventory.removeItem(itemName);
    }
   
    /**
     * Metoda vrací název příkazu.
     * @return název příkazu
     */
    @Override
    public String getName() {
        return NAME;
    }
}