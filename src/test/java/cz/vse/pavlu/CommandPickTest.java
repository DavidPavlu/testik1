package cz.vse.pavlu;

import cz.vse.pavlu.model.Area;
import cz.vse.pavlu.model.Game;
import cz.vse.pavlu.model.Inventory;
import cz.vse.pavlu.model.Item;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída pro komplexní otestování příkazu "seber".
 *
 * @author David Pavlů
 * @version LS 2020
 */
public class CommandPickTest
{
    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testCommandPick() {
        Area area = new Area("hala", "Toto je vstupní hala budovy VŠE na Jižním městě.");
        Inventory inventory = new Inventory(game.getGamePlan());

        Item item1 = new Item("stul", "Těžký dubový stůl.", false);
        Item item2 = new Item("rum", "Láhev vyzrálého rumu.");
        
        area.addItem(item1);
        area.addItem(item2);
        
        game.processCommand("seber stul");
        assertFalse(item1.isMoveable());
        assertTrue(area.containsItem(item1.getName()));
        assertFalse(inventory.containsItem(item1.getName()));
        
        game.processCommand("seber rum");
        inventory.addToInventory(item2.getName(), item2);
        area.removeItem(item2.getName());
        assertTrue(item2.isMoveable());
        assertFalse(area.containsItem(item2.getName()));
        assertTrue(inventory.containsItem(item2.getName()));
    }
}
