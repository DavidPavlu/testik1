package cz.vse.pavlu.model;

/**
 * Třída implementující příkaz pro prozkoumání lokace - výpis předmětů z lokace.
 * @author David Pavlů
 * @version LS 2020
 */
public class CommandInspectLocation implements ICommand
{
    private static final String NAME = "prohledej";
    private GamePlan plan;
    
    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandInspectLocation(GamePlan plan) {
        this.plan = plan;
    }
    
    /**
     * Metoda vrací výpis předmětů z aktuální lokace.
     * @param parameters parametry příkazu (očekává se pole s jedním prvkem)
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    @Override
    public String process(String... parameters) {
        String itemNames = "Předměty:";
        for (String itemName : plan.getCurrentArea().getItems().keySet()) {
            itemNames += " " + itemName;
        }
        
        for (String chestName : plan.getCurrentArea().getChests().keySet()) {
            itemNames += " " + chestName;
        }
    
        String enemyNames = "Osoby:";
        for (String enemyName : plan.getCurrentArea().getEnemies().keySet()) {
            enemyNames += " " + enemyName;
        }
        
        return itemNames + "\n" + enemyNames;
    }
    
    /**
     * Metoda vrací název příkazu.
     * @return název příkazu
     */
    @Override
    public String getName() {
        return NAME;
    }
}