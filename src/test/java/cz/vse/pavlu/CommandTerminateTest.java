package cz.vse.pavlu;

import cz.vse.pavlu.model.Game;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída pro komplexní otestování příkazu "konec".
 *
 * @author David Pavlů
 * @version LS 2020
 */
public class CommandTerminateTest
{
    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testCommandTerminate() {
        game.processCommand("pomoc");
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi meereen");
        assertFalse(game.isGameOver());
        
        game.processCommand("seber mesec");
        assertFalse(game.isGameOver());
        
        game.processCommand("konec");
        assertTrue(game.isGameOver());
    }
}
