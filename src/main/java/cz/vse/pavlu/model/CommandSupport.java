package cz.vse.pavlu.model;

/**
 * Třída implementující příkaz pro zobrazení příkazů.
 *
 * @author David Pavlů
 * @version LS 2020
 */
public class CommandSupport implements ICommand
{
    private static final String NAME = "pomoc";

    private ListOfCommands listOfCommands;

    /**
     * Konstruktor třídy.
     *
     * @param listOfCommands odkaz na seznam příkazů, které je možné ve hře použít
     */
    public CommandSupport(ListOfCommands listOfCommands) {
        this.listOfCommands = listOfCommands;
    }

    /**
     * Metoda vrací obecnou nápovědu ke hře. Nyní vypisuje vcelku primitivní
     * zprávu o herním příběhu a seznam dostupných příkazů, které může hráč
     * používat.
     *
     * @param parameters parametry příkazu (aktuálně se ignorují)
     * @return nápověda, která se vypíše na konzoli
     */
    @Override
    public String process(String... parameters) {
        return "Ve hře můžeš používat tyto příkazy:\n" + listOfCommands.getNames();
    }

    /**
     * Metoda vrací název příkazu.
     * @return název příkazu
     */
    @Override
    public String getName() {
        return NAME;
    }
}
