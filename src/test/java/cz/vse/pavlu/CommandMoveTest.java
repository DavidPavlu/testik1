package cz.vse.pavlu;

import cz.vse.pavlu.model.Game;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Testovací třída pro komplexní otestování příkazu "jdi".
 *
 * @author David Pavlů
 * @version LS 2020
 */
public class CommandMoveTest
{
    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testCommandMove() {
        assertEquals("meereen", game.getGamePlan().getCurrentArea().getName());
        game.processCommand("jdi meereen");
        assertEquals("meereen", game.getGamePlan().getCurrentArea().getName());

        game.processCommand("jdi dorne");
        assertEquals("dorne", game.getGamePlan().getCurrentArea().getName());

        game.processCommand("jdi riverrun");
        assertEquals("riverrun", game.getGamePlan().getCurrentArea().getName());
        
        game.processCommand("jdi domecek");
        assertNull(game.getGamePlan().getCurrentArea().getExitArea("domecek"));
    }
}
