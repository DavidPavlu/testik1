package cz.vse.pavlu;

import cz.vse.pavlu.model.Area;
import cz.vse.pavlu.model.Chest;
import cz.vse.pavlu.model.Game;
import cz.vse.pavlu.model.Item;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída pro komplexní otestování truhly.
 *
 * @author David Pavlů
 * @version LS 2020
 */
public class ChestTest
{
    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testEmptyChest() {
        Area area = new Area("hala", "Toto je vstupní hala budovy VŠE na Jižním městě.");
        Chest chest = new Chest("truhla", "Zamčená velká truhla.", "sperhak");
        
        Item item1 = new Item("rum", "Láhev vyzrálého rumu.");
        Item item2 = new Item("chleb", "Starý tvrdý chléb.");
        
        area.addChest(chest);
        chest.addToChest(item1.getName(), item1);
        chest.addToChest(item2.getName(), item2);      
        
        assertTrue(area.containsChest(chest.getName()));
        assertFalse(area.containsItem(item1.getName()));
        assertFalse(area.containsItem(item2.getName()));
        
        chest.emptyChest(chest.getName());
        assertFalse(game.getGamePlan().getCurrentArea().containsChest(chest.getName()));
        assertTrue(game.getGamePlan().getCurrentArea().containsItem(item1.getName()));
        assertTrue(game.getGamePlan().getCurrentArea().containsItem(item2.getName()));
    }
    
    @Test
    public void testChestExists() {
        Chest chest = new Chest("truhla", "Zamčená velká truhla.", "sperhak");

        assertTrue(chest.chestExists(chest.getName()));
        assertFalse(chest.chestExists("truhlicka"));
    }
}
