package cz.vse.pavlu.model;

/**
 * Hlavní třída logiky aplikace. Třída vytváří instanci třídy {@link GamePlan},
 * která inicializuje lokace hry, a vytváří seznam platných příkazů a instance
 * tříd provádějících jednotlivé příkazy.
 *
 * Během hry třída vypisuje uvítací a ukončovací texty a vyhodnocuje jednotlivé
 * příkazy zadané uživatelem.
 *
 * @author Michael Kölling
 * @author Luboš Pavlíček
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author David Pavlů
 * @version LS 2020
 */
public class Game implements IGame
{
    private ListOfCommands listOfCommands;
    private GamePlan gamePlan;
    private boolean gameOver;

    /**
     * Konstruktor třídy. Vytvoří hru, inicializuje herní plán udržující
     * aktuální stav hry a seznam platných příkazů.
     */
    public Game() {
        gameOver = false;
        gamePlan = new GamePlan();
        listOfCommands = new ListOfCommands();
        Inventory inventory = new Inventory(gamePlan);

        listOfCommands.addCommand(new CommandHelp());
        listOfCommands.addCommand(new CommandTerminate(this));
        listOfCommands.addCommand(new CommandMove(gamePlan));
        listOfCommands.addCommand(new CommandPick(gamePlan, inventory));
        listOfCommands.addCommand(new CommandInspect(gamePlan, inventory));
        listOfCommands.addCommand(new CommandInspectLocation(gamePlan));
        listOfCommands.addCommand(new CommandSupport(listOfCommands));
        listOfCommands.addCommand(new CommandShowInventory(inventory));
        listOfCommands.addCommand(new CommandRemoveItem(inventory));
        listOfCommands.addCommand(new CommandTalk(gamePlan));
        listOfCommands.addCommand(new CommandKill(gamePlan, inventory));
        listOfCommands.addCommand(new CommandOpen(gamePlan, inventory));
    }

    /**
     * Metoda vrací počáteční text a popis počáteční oblasti.
     * @retrun počáteční text
     */
    @Override
    public String getPrologue() {
        return "Vítejte!\n"
                + "Toto je cesta Aryi Stark, tvým úkolem je projít oblasti, najít důležité předměty\na na konci zabít Nočního krále.\n"
                + "Napište 'napoveda', pokud si nevíte rady, jak hrát dál.\n"
                + "\n"
                + gamePlan.getCurrentArea().getFullDescription();
    }

    /**
     * Metoda vrací konečný text při výhře.
     * @return konečný text
     */
    @Override
    public String getEpilogue() {
        String epilogue = "Díky, že sis zahrál(a).";

        if (gamePlan.isVictorious()) {
            epilogue = "ZVÍTĚZIL(A) JSI - zabil si Nočního krále!\n\n" + epilogue;
        }
        
        if (gamePlan.isDefeated()) {
            epilogue = "PROHRÁL(A) JSI - Noční král tě zabil!\n\n" + epilogue;
        }

        return epilogue;
    }

    /**
     * Metoda vrací stav hry - je/není ukončena.
     * @return konec hry (true/false)
     */
    @Override
    public boolean isGameOver() {
        return gameOver;
    }

    /**
     * Metoda vyhodnocuje a vrací příkazy (plus parametry) zadené hráčem.
     * @return příkaz/chybové hlášení
     * @param line příkaz zadaný do konzole
     */
    @Override
    public String processCommand(String line) {
        String[] words = line.split("[ \t]+");

        String cmdName = words[0];
        String[] cmdParameters = new String[words.length - 1];

        for (int i = 0; i < cmdParameters.length; i++) {
            cmdParameters[i] = words[i + 1];
        }

        String result = null;
        if (listOfCommands.checkCommand(cmdName)) {
            ICommand command = listOfCommands.getCommand(cmdName);
            result = command.process(cmdParameters);
        } else {
            result = "Nechápu, co po mně chceš. Tento příkaz neznám.";
        }

        if (gamePlan.isVictorious()) {
            gameOver = true;
        }
        
        if (gamePlan.isDefeated()) {
            gameOver = true;
        }

        return result;
    }

    /**
     * Metoda vrací kompletní sestavený herní plán ze třídy GamePlan.
     * @return herní plán
     */
    @Override
    public GamePlan getGamePlan() {
        return gamePlan;
    }

    /**
     * Metoda nastaví příznak indikující, že nastal konec hry. Metodu
     * využívá třída {@link CommandTerminate}, mohou ji ale použít
     * i další implementace rozhraní {@link ICommand}.
     *
     * @param gameOver příznak indikující, zda hra již skončila
     */
    void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }
}
