package cz.vse.pavlu.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Třída implementující truhlu, konstruktor a potřebné metody pro práci s truhlou.
 * @author David Pavlů
 * @version LS 2020
 */
public class Chest
{
    private String name;
    private String description;
    private String opener;
    private Map<String, Item> chestMap = new HashMap<String, Item>();
    private GamePlan plan;
    
    /**
     * Konstruktor třídy
     * @param name
     * @param description
     * @param opener
     */
    public Chest(String name, String description, String opener) {
        this.name = name;
        this.description = description;
        this.opener = opener;
        this.chestMap = new HashMap<String, Item>();
    }
    
    /**
     * Metoda přidává předmět do truhly.
     * @param name název předmětu
     * @param item
     */
    public void addToChest(String name, Item item) {
        this.chestMap.put(name, item);    
    }
     
    /**
     * Metoda získá předměty z truhly.
     * @return předměty z truhly
     */
    public Map<String, Item> getChest() {
        return this.chestMap;
    }
    
    /**
     * Metoda vloží předměty z truhly do aktuální lokace a smaže ji.
     * @param chestName
     */
    public void emptyChest(String chestName) {  
        for(Item item: this.chestMap.values()) {     
            plan.getCurrentArea().addItem(item);
        }
        
        plan.getCurrentArea().removeChest(chestName);
    }
    
    /**
     * Metoda zjišťuje, zda truhla existuje.
     * @param itemName název předmetu
     * @return podle stavu inventáře true/false
     */
    public boolean chestExists(String itemName) {             
        if(name.equals(itemName)) {
            return true;
        }

        return false;
    }
    
    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }
    
    public String getOpener() {
        return this.opener;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}