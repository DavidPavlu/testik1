package cz.vse.pavlu.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Třída implementující inventář, konstruktor a potřebné metody pro práci s inventářem.
 * @author David Pavlů
 * @version LS 2020
 */
public class Inventory
{
    private Map<String, Item> inventoryMap;
    private GamePlan plan;
    
    /**
     * Konstruktor třídy
     * @param plan herní plán
     */
    public Inventory(GamePlan plan) {
        this.inventoryMap = new HashMap<String, Item>();
        this.plan = plan;
    }
    
    /**
     * Metoda kontroluje plnost invetáře a následně přidává předmět.
     * @param name název předmětu
     * @param item
     * @return podle stavu inventáře a provedení true/false
     */
    public boolean addToInventory(String name, Item item) {
        if(this.inventoryMap.size() < 2){
            this.inventoryMap.put(name, item);
            return true;
        }
        return false;
    }
    
    /**
     * Metoda zjišťuje, zda je předmět v inventáři.
     * @param itemName název předmetu
     * @return podle stavu inventáře true/false
     */
    public boolean containsItem(String itemName) {             
        for(Item item: this.inventoryMap.values()) {
            if(item.getName().equals(itemName)) {
                return true;
            }
        }

        return false;
    }
    
    /**
     * Metoda odebere předmět z inventáře a umístí ho do aktuální lokace.
     * @param itemName název předmětu
     * @return informační text
     */
    public String removeItem(String itemName) {     
        boolean itemExists = false;
        
        for(Item item: this.inventoryMap.values()) { 
            if(item.getName().equals(itemName)) {
                plan.getCurrentArea().addItem(item);
                itemExists = true;
            }
        }
        
        if (itemExists) {
            inventoryMap.remove(itemName);
            return "Vyhodil si " + itemName + " z inventáře.";
        }
        
        return "Tento předmět nemáš v invetáři.";
    }
    
    /**
     * Metoda odebere předmět z inventáře a aktuální hry.
     * @param itemName název předmětu
     */
    public void removeItemTotally(String itemName) {     
        for(Item item: this.inventoryMap.values()) { 
            if(item.getName().equals(itemName)) {
                inventoryMap.remove(itemName);
            }
        }
    }
    
    /**
     * Metoda získá předmět z inventáře.
     * @param itemName název předmetu
     * @return název předmětu
     */
    public Item getItem(String itemName) {             
        return inventoryMap.get(itemName);
    }
    
    /**
     * Metoda získá předměty z inventáře.
     * @return předměty z inventáře
     */
    public Map<String, Item> getInventory() {
        return this.inventoryMap;
    }
}