package cz.vse.pavlu.model;

/**
 * Třída implementující příkaz pro zobrazení nápovědy ke hře.
 *
 * @author Jarmila Pavlíčková
 * @author Luboš Pavlíček
 * @author Jan Říha
 * @author David Pavlů
 * @version LS 2020
 */
public class CommandHelp implements ICommand
{
    private static final String NAME = "napoveda";

    /**
     * Metoda vrací obecnou nápovědu ke hře. Nyní vypisuje vcelku primitivní
     * zprávu o herním příběhu a seznam dostupných příkazů, které může hráč
     * používat.
     *
     * @param parameters parametry příkazu (aktuálně se ignorují)
     * @return nápověda, která se vypíše na konzoli
     */
    @Override
    public String process(String... parameters) {
        return "Tvým úkolem je projít připravené oblasti, najít důležité předměty\n"
                + "a zabít Nočního krále.\n"
                + "Pro zobrazení příkazů napiš: pomoc\n";
    }

    /**
     * Metoda vrací název příkazu.
     * @return název příkazu
     */
    @Override
    public String getName() {
        return NAME;
    }
}
