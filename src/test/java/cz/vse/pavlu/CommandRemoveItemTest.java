package cz.vse.pavlu;

import cz.vse.pavlu.model.Game;
import cz.vse.pavlu.model.Inventory;
import cz.vse.pavlu.model.Item;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída pro komplexní otestování příkazu "poloz".
 *
 * @author David Pavlů
 * @version LS 2020
 */
public class CommandRemoveItemTest
{
    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testCommandRemoveItem() {
        //odebere z inventu, da na zem - vytvorit invent, neosahuje, vytvorit item, obsahuje, odebrat z inventu, neobsahuje v inventu, obasuje v lokaci
        Inventory inventory = new Inventory(game.getGamePlan());

        Item item1 = new Item("rum", "Láhev vyzrálého rumu.");
        
        inventory.addToInventory(item1.getName(), item1);
        
        assertTrue(inventory.containsItem(item1.getName()));
        assertFalse(game.getGamePlan().getCurrentArea().containsItem(item1.getName()));
        
        game.processCommand("poloz rum");
        inventory.removeItem(item1.getName());
        
        assertFalse(inventory.containsItem(item1.getName()));
        assertTrue(game.getGamePlan().getCurrentArea().containsItem(item1.getName()));
    }
}
