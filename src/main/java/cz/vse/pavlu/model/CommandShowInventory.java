package cz.vse.pavlu.model;

/**
 * Třída implementující příkaz pro výpis předmětů z inventáře.
 * @author David Pavlů
 * @verison LS 2020
 */
public class CommandShowInventory implements ICommand
{
    private static final String NAME = "inventar";
    private Inventory inventory;
    
    /**
     * Konstruktor třídy.
     * @param inventory
     */
    public CommandShowInventory(Inventory inventory) {
        this.inventory = inventory;
    }
    
    /**
     * Metoda vrací výpis předmětů z inventáře.
     * @param parameters parametry příkazu (očekává se pole s jedním prvkem)
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    @Override
    public String process(String... parameters) {
        String itemNames = "Předměty v inventáři:";
        for (String itemName : inventory.getInventory().keySet()) {
            itemNames += " " + itemName;
        }
        
        return itemNames;
    }
   
    /**
     * Metoda vrací název příkazu.
     * @return název příkazu
     */
    @Override
    public String getName() {
        return NAME;
    }
}