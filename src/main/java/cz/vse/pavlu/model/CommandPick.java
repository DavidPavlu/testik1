package cz.vse.pavlu.model;

/**
 * Třída implementující příkaz pro sebrání předmětu a vložení do inventáře.
 * @author David Pavlů
 * @verion LS 2020
 */
public class CommandPick implements ICommand
{
    private static final String NAME = "seber";
    private Inventory inventory;    
    private GamePlan plan;
    
    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     * @param inventory odkaz na aktuální inventář
     */
    public CommandPick(GamePlan plan, Inventory inventory) {
        this.plan = plan;
        this.inventory = inventory;
    }
    
    /**
     * Metoda kontroluje možnost sebrání vybraného předmětu (v parametru) a ukládá ho do inventáře, pokud je místo.
     * @param parameters parametry příkazu (očekává se pole s jedním prvkem)
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    @Override
    public String process(String... parameters) {
        if (parameters.length == 0) {
            return "Nevím, co mám sebrat, musíš zadat název předmětu.";
        }
        
        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím sebrat více předmětů současně.";
        }
        
        String itemName = parameters[0];
        Area area = plan.getCurrentArea();
        
        if (!area.containsItem(itemName)) {
            return "Předmět '" + itemName + "' tady není.";
        }
        
        Item item = area.getItem(itemName);
        
        if (!item.isMoveable()) {
            return "Předmět '" + itemName + "' fakt neuneseš.";
        }

        boolean deleteItem = true;
        deleteItem = inventory.addToInventory(itemName, item);
                
        if(deleteItem){
            area.removeItem(itemName);
            return "Sebral(a) jsi předmět '" + itemName + "' a uložil jsi ho do inventáře.";
        }

        return "Více předmětů neuneseš, musíš něco vyhodit.";
    }
    
    /**
     * Metoda vrací název příkazu.
     * @return název příkazu
     */
    @Override
    public String getName() {
        return NAME;
    }
}
