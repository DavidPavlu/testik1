package cz.vse.pavlu;

import cz.vse.pavlu.model.Area;
import cz.vse.pavlu.model.Game;
import cz.vse.pavlu.model.Inventory;
import cz.vse.pavlu.model.Item;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída pro komplexní otestování příkazu "prozkoumej".
 *
 * @author David Pavlů
 * @version LS 2020
 */
public class CommandInspectTest
{
    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testCommandInspect() {
        // item v aree, item v inventu
        Area area = new Area("hala", "Toto je vstupní hala budovy VŠE na Jižním městě.");
        Inventory inventory = new Inventory(game.getGamePlan());

        Item item1 = new Item("stul", "Těžký dubový stůl.", false);
        Item item2 = new Item("rum", "Láhev vyzrálého rumu.");
        Item item3 = new Item("chleb", "Starý tvrdý chléb.");
        
        area.addItem(item1);
        inventory.addToInventory(item2.getName(), item2);
        
        
        game.processCommand("prozkoumej stul");
        assertTrue(area.containsItem(item1.getName()));
        assertFalse(inventory.containsItem(item1.getName()));
        
        game.processCommand("prozkoumej rum"); 
        assertFalse(area.containsItem(item2.getName()));
        assertTrue(inventory.containsItem(item2.getName()));
        
        game.processCommand("prozkoumej chleb"); 
        assertFalse(area.containsItem(item3.getName()));
        assertFalse(inventory.containsItem(item3.getName()));
    }
}
