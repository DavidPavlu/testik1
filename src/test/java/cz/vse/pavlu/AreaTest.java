package cz.vse.pavlu;

import cz.vse.pavlu.model.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída pro komplexní otestování třídy {@link Area}.
 *
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author David Pavlů
 * @version LS 2020
 */
public class AreaTest
{
    @Test
    public void testAreaExits()
    {
        Area area1 = new Area("hala", "Toto je vstupní hala budovy VŠE na Jižním městě.");
        Area area2 = new Area("bufet", "Toto je bufet, kam si můžete zajít na svačinu.");

        area1.addExit(area2);
        area2.addExit(area1);

        assertEquals(area1, area2.getExitArea(area1.getName()));
        assertEquals(area2, area1.getExitArea(area2.getName()));

        assertNull(area1.getExitArea("pokoj"));
        assertNull(area2.getExitArea("pokoj"));
    }

    @Test
    public void testItems()
    {
        Area area = new Area("hala", "Toto je vstupní hala budovy VŠE na Jižním městě.");

        Item item1 = new Item("stul", "Těžký dubový stůl.", false);
        Item item2 = new Item("rum", "Láhev vyzrálého rumu.");

        assertFalse(area.containsItem(item1.getName()));
        assertFalse(area.containsItem(item2.getName()));
        assertFalse(area.containsItem("pc"));

        area.addItem(item1);
        area.addItem(item2);

        assertEquals(item1, area.getItem(item1.getName()));
        assertEquals(item2, area.getItem(item2.getName()));
        assertNull(area.getItem("pc"));

        assertTrue(area.containsItem(item1.getName()));
        assertTrue(area.containsItem(item2.getName()));
        assertFalse(area.containsItem("pc"));

        assertEquals(item1, area.removeItem(item1.getName()));
        assertEquals(item2, area.removeItem(item2.getName()));
        assertNull(area.removeItem("pc"));

        assertFalse(area.containsItem(item1.getName()));
        assertFalse(area.containsItem(item2.getName()));
        assertFalse(area.containsItem("pc"));
    }
    
    @Test
    public void testEnemies()
    {
        Area area = new Area("hala", "Toto je vstupní hala budovy VŠE na Jižním městě.");

        Enemy enemy1 = new Enemy("terorista", "Terorista se zbraní.", true);
        Enemy enemy2 = new Enemy("civilista", "Civilista beze zbraně.");

        assertFalse(area.containsEnemy(enemy1.getName()));
        assertFalse(area.containsEnemy(enemy2.getName()));
        assertFalse(area.containsEnemy("jarda"));

        area.addEnemy(enemy1);
        area.addEnemy(enemy2);

        assertEquals(enemy1, area.getEnemy(enemy1.getName()));
        assertEquals(enemy2, area.getEnemy(enemy2.getName()));
        assertNull(area.getEnemy("jarda"));

        assertTrue(area.containsEnemy(enemy1.getName()));
        assertTrue(area.containsEnemy(enemy2.getName()));
        assertFalse(area.containsEnemy("jarda"));
    }
    
    @Test
    public void testChests()
    {
        GamePlan plan = new GamePlan();
        Area area = new Area("hala", "Toto je vstupní hala budovy VŠE na Jižním městě.");

        Chest chest = new Chest("truhla", "Zamčená velká truhla.", "sperhak");

        assertFalse(area.containsChest(chest.getName()));
        assertFalse(area.containsChest("truhlicka"));

        area.addChest(chest);

        assertEquals(chest, area.getChest(chest.getName()));
        assertNull(area.getChest("truhlicka"));

        assertTrue(area.containsChest(chest.getName()));
        assertFalse(area.containsChest("truhlicka"));

        assertEquals(chest, area.removeChest(chest.getName()));
        assertNull(area.removeChest("truhlicka"));

        assertFalse(area.containsChest(chest.getName()));
        assertFalse(area.containsChest("truhlicka"));
    }
}
