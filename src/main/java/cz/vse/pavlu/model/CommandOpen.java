package cz.vse.pavlu.model;

/**
 * Třída implementující příkaz pro otevření truhly.
 * @author David Pavlů
 * @version LS 2O2O
 */
public class CommandOpen implements ICommand
{
    private static final String NAME = "otevri";
    
    private GamePlan plan;
    private Inventory inventory;
    
    /**
     * Konstruktor třídy.
     * @param plan odkaz na herní plán s aktuálním stavem hry
     * @param inventory odkaz na aktuální inventář
     */
    public CommandOpen(GamePlan plan, Inventory inventory) {
        this.plan = plan;
        this.inventory = inventory;
    }
    
    /**
     * Metoda zjišťuje zda truhlu otevřeme a následně tak udělá, pokud budou splněny podmínky
     * (truhla je v naší aktuální lokaci + vlastníme šperhák)
     * @param parameters parametry příkazu (očekává se pole s jedním prvkem)
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    public String process(String... parameters) {
        if (parameters.length == 0) {
            return "Nevím, co mám otevřít, musíš zadat název truhly.";
        }
        
        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím otevřít více truhel současně.";
        }
        
        String chestName = parameters[0];
        Area area = plan.getCurrentArea();
         
        if (area.containsChest(chestName) && inventory.containsItem(area.getChest(chestName).getOpener())) {
            
            
            area.getChest(chestName).emptyChest(chestName);
            inventory.removeItemTotally(area.getChest(chestName).getOpener());
            return "Zběsile si otevřel truhlu a předměty se rozsypaly všude po zemi, podívej se po nich.";
        }
       
        return "Truhlu nemáš čím otevřít nebo tento předmět otevřít nejde.";
    }
    
    /**
     * Metoda vrací název příkazu.
     * @return název příkazu
     */
    public String getName() {
        return NAME;
    }
}
