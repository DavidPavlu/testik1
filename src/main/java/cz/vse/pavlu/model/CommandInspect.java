package cz.vse.pavlu.model;

/**
 * Třída implementující příkaz pro prozkoumání předmětu.
 * @author David Pavlů
 * @version LS2020
 */
public class CommandInspect implements ICommand
{
    private static final String NAME = "prozkoumej";
    
    private GamePlan plan;
    private Inventory inventory;
    
    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     * @param inventory odkaz na aktuální inventář
     */
    public CommandInspect(GamePlan plan, Inventory inventory) {
        this.plan = plan;
        this.inventory = inventory;
    }
    
    /**
     * Metoda vypíše popis předmětu z lokace nebo inventáře,
     * podle názvu předmětu, který chceme prozkoumat.
     * @param parameters parametry příkazu (očekává se pole s jedním prvkem)
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    public String process(String... parameters) {
        if (parameters.length == 0) {
            return "Nevím, co mám prozkoumat, musíš zadat název předmětu.";
        }
        
        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím prozkoumat více předmětů současně.";
        }
        
        String itemName = parameters[0];
        Area area = plan.getCurrentArea();
         
        if (!area.containsItem(itemName) && !inventory.containsItem(itemName) && !area.containsChest(itemName)) {
            return itemName + " momentálně nelze prozkoumat.";
        }
        
        if (area.containsItem(itemName)) {
            return area.getItem(itemName).getDescription();
        }
        
        if (inventory.containsItem(itemName)) {
            return inventory.getItem(itemName).getDescription();
        }
        
        if (area.getChest(itemName).getName().equals(itemName)) {
            return area.getChest(itemName).getDescription();
        }
        
        return "Něco se pokazilo.";
    }
    
    /**
     * Metoda vrací název příkazu.
     * @return název příkazu
     */
    public String getName() {
        return NAME;
    }
}
