package cz.vse.pavlu;

import cz.vse.pavlu.model.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída pro komplexní otestování příkazu "zabij".
 *
 * @author David Pavlů
 * @version LS 2020
 */
public class CommandKillTest
{
    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testCommandKill() {
        //Metoda zjišťuje náležitosi pro možné zabítí správného nepřítele, taktéž rozhoduje o výhře či prohře.
        //enemy je nightKing, je v lokaci, v inventu je mec
        Area area = new Area("hala", "Toto je vstupní hala budovy VŠE na Jižním městě.");
        Inventory inventory = new Inventory(game.getGamePlan());
        Enemy enemy1 = new Enemy("terorista", "Terorista se zbraní.", true);
        Enemy enemy2 = new Enemy("civilista", "Civilista beze zbraně.");
        Item item = new Item("mec", "Meč k zabití nepřítele.");

        game.processCommand("zabij civilista");
        assertFalse(inventory.containsItem(item.getName()));
        assertFalse(area.containsEnemy(enemy2.getName()));
        assertFalse(enemy2.isKillable());
        
        inventory.addToInventory(item.getName(), item);
        
        game.processCommand("zabij terorista");
        assertTrue(inventory.containsItem(item.getName()));
        assertFalse(area.containsEnemy(enemy1.getName()));
        assertTrue(enemy1.isKillable());
        
        area.addEnemy(enemy1);
        
        game.processCommand("zabij terorista");
        assertTrue(inventory.containsItem(item.getName()));
        assertTrue(area.containsEnemy(enemy1.getName()));
        assertTrue(enemy1.isKillable());
    }
}
