package cz.vse.pavlu;

import cz.vse.pavlu.model.Area;
import cz.vse.pavlu.model.Enemy;
import cz.vse.pavlu.model.Game;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída pro komplexní otestování příkazu "oslov".
 *
 * @author David Pavlů
 * @version LS 2020
 */
public class CommandTalkTest
{
    private Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testCommandTalk() {
        Area area = new Area("hala", "Toto je vstupní hala budovy VŠE na Jižním městě.");

        Enemy enemy = new Enemy("civilista", "Civilista beze zbraně.");
        
        game.processCommand("oslov civilista");
        assertFalse(area.containsEnemy(enemy.getName()));

        area.addEnemy(enemy);
        
        game.processCommand("oslov civilista");
        assertEquals(enemy, area.getEnemy(enemy.getName()));
        assertTrue(area.containsEnemy(enemy.getName()));
    }
}
