package cz.vse.pavlu.model;

/**
 * Třída implementující věc, její parametry, metody a konstruktor.
 * @author David Pavlů
 * @version LS 2020
 */
public class Item
{
    private String name;
    private String description;
    private boolean moveable;
    
    /**
     * Konstruktor třídy.
     * @param name název
     * @param description popis
     * @param moveable přesouvatelnost
     */
    public Item(String name, String description, boolean moveable) {
        this.name = name;
        this.description = description;
        this.moveable = moveable;
    }

    public Item(String name, String description) {
        this(name, description, true);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isMoveable() {
        return moveable;
    }

    public void setMoveable(boolean moveable) {
        this.moveable = moveable;
    }
}
