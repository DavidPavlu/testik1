package cz.vse.pavlu.model;

/**
 * Třída implementující příkaz pro zabití nepřítele.
 * @author David Pavlů
 * @version LS 2O2O
 */
public class CommandKill implements ICommand
{
    private static final String NAME = "zabij";

    private GamePlan plan;
    private Inventory inventory;

    /**
     * Konstruktor třídy.
     * @param plan odkaz na herní plán s aktuálním stavem hry
     * @param inventory inventář
     */
    public CommandKill(GamePlan plan, Inventory inventory) {
        this.plan = plan;
        this.inventory = inventory;
    }

    /**
     * Metoda zjišťuje náležitosi pro možné zabítí správného nepřítele, taktéž rozhoduje o výhře či prohře.
     * @param parameters parametry příkazu (očekává se pole s jedním prvkem)
     * @return informace pro hráče, které se vypíšou na konzoli
     */
    public String process(String... parameters) {
        if (parameters.length == 0) {
            return "Nevím, koho mám zábít, musíš zadat název osoby.";
        }

        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím zabít více osob současně.";
        }

        String enemyName = parameters[0];
        Area area = plan.getCurrentArea();
        
        if (area.getEnemy(enemyName) == null) {
            return "Tato postava tady není.";
        } 
        
        if (!area.getEnemy(enemyName).isKillable()) {
            return "Tato postava zabít nelze, zkus jí radši oslovit.";
        }

        if (area.containsEnemy(enemyName) && area.getEnemy(enemyName).isKillable() && inventory.containsItem("mec")) {
            plan.isWin = true;
        }

        if (area.containsEnemy(enemyName) && area.getEnemy(enemyName).isKillable() && !inventory.containsItem("mec")) {
            plan.isLose = true;
        }

        return "Boj dopadl následovně:";
    }

    /**
     * Metoda vrací název příkazu.
     * @return název příkazu
     */
    public String getName() {
        return NAME;
    }
}
