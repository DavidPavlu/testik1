package cz.vse.pavlu;

import cz.vse.pavlu.model.Game;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída pro komplexní otestování herního příběhu.
 *
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author David Pavlů
 * @version LS 2020
 */
public class GameTest
{
    private Game game;

    @Before
    public void setUp()
    {
        game = new Game();
    }
    
    @After
    public void tearDown()
    {
        game = new Game();
    }

    @Test
    public void testPlayerQuit()
    {   
        assertEquals("meereen", game.getGamePlan().getCurrentArea().getName());

        game.processCommand("jdi dorne");
        assertEquals("dorne", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());

        game.processCommand("jdi riverrun");
        assertEquals("riverrun", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());

        game.processCommand("konec");
        assertTrue(game.isGameOver());
    }
    
    @Test
    public void testPlayerWin()
    {        
        assertEquals("meereen", game.getGamePlan().getCurrentArea().getName());

        game.processCommand("jdi dorne");
        assertFalse(game.isGameOver());
        
        game.processCommand("seber sperhak");
        assertFalse(game.isGameOver());

        game.processCommand("jdi riverrun");
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi winterFell");
        assertFalse(game.isGameOver());
        
        game.processCommand("otevri truhlaJonaSnow");
        assertFalse(game.isGameOver());
        
        game.processCommand("seber mec");
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi theWall");
        assertFalse(game.isGameOver());

        game.processCommand("zabij nightKing"); 
        assertTrue(game.getGamePlan().isVictorious());
        assertTrue(game.isGameOver());
    }
    
    @Test
    public void testPlayerLose()
    {        
        assertEquals("meereen", game.getGamePlan().getCurrentArea().getName());

        game.processCommand("jdi dorne");
        assertFalse(game.isGameOver());
        
        game.processCommand("seber sperhak");
        assertFalse(game.isGameOver());

        game.processCommand("jdi riverrun");
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi winterFell");
        assertFalse(game.isGameOver());
        
        game.processCommand("otevri truhlaJonaSnow");
        assertFalse(game.isGameOver());
        
        //nesebere mec - game.processCommand("seber mec");
        
        game.processCommand("jdi theWall");
        assertFalse(game.isGameOver());

        game.processCommand("zabij nightKing");
        assertTrue(game.getGamePlan().isDefeated());
        assertTrue(game.isGameOver());
    }
}
