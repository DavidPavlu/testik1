package cz.vse.pavlu.model;

/**
 * Třída implementující osobu/nepřítele, její parametry, metody a konstruktor.
 * @author David Pavlů
 * @version LS 2020
 */
public class Enemy
{
    private String name;
    private String description;
    private boolean killable;

    /**
     * Konstruktor třídy.
     * @param name název
     * @param description popis
     * @param killable zabitelnost
     */
    public Enemy(String name, String description, boolean killable) {
        this.name = name;
        this.description = description;
        this.killable = killable;
    }

    public Enemy(String name, String description) {
        this(name, description, false);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isKillable() {
        return killable;
    }

    public void setKillable(boolean killable) {
        this.killable = killable;
    }
}
