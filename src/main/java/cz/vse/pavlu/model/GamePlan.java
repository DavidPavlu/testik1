package cz.vse.pavlu.model;

/**
 * Třída představující aktuální stav hry. Veškeré informace o stavu hry
 * <i>(mapa lokací, inventář, vlastnosti hlavní postavy, informace o plnění
 * úkolů apod.)</i> by měly být uložené zde v podobě datových atributů.
 * <p>
 * Třída existuje především pro usnadnění potenciální implementace ukládání
 * a načítání hry. Pro uložení rozehrané hry do souboru by mělo stačit uložit
 * údaje z objektu této třídy <i>(např. pomocí serializace objektu)</i>. Pro
 * načtení uložené hry ze souboru by mělo stačit vytvořit objekt této třídy
 * a vhodným způsobem ho předat instanci třídy {@link Game}.
 *
 * @author Michael Kölling
 * @author Luboš Pavlíček
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author David Pavlů
 * @version LS 2020
 *
 * @see <a href="https://java.vse.cz/4it101/AdvSoubory">Postup pro implementaci ukládání a načítání hry na předmětové wiki</a>
 * @see java.io.Serializable
 */
public class GamePlan
{
    public boolean isWin = false;
    public boolean isLose = false;

    private Area currentArea;

    /**
     * Konstruktor třídy. Pomocí metody {@link #prepareWorldMap() prepareWorldMap}
     * vytvoří jednotlivé lokace a propojí je pomocí východů.
     */
    public GamePlan() {
        prepareWorldMap();
    }

    /**
     * Metoda vytváří jednotlivé lokace, předměty a osoby. Propojuje je pomocí východů a umisťuje do lokací. 
     * Jako výchozí aktuální lokaci následně nastaví meereen, ve kterém hra začíná.
     */
    private void prepareWorldMap() {
        // Vytvoříme předmety
        Item mesec = new Item("mesec", "Měšec plný pravých mincí.");
        Item sperhak = new Item("sperhak", "Šperhák složící k otevření zamčené truhly.");
        Item vino = new Item("vino", "Zkažené smradlavé víno.");
        Item chleb = new Item("chleb", "Starý tvrdý chléb.");
        Item mec = new Item("mec", "Malý meč připomínájící jehlu zvaný Needle.");
        Item padlo = new Item("padlo", "Dřevěné pádlo z rozbité lodi.");
        Item pisek = new Item("pisek", "Spousta písku k ničemu.", false);
        Item zeleznyTrun = new Item("zeleznyTrun", "TEN Železný trůn.", false);
        Item lod = new Item("lod", "Rozbitá loď.", false);
        Item socha = new Item("socha", "Železná socha rodu Greyjoyů.", false);

        // Vytvoříme truhlu
        Chest truhlaJonaSnow = new Chest("truhlaJonaSnow", "Truhla Jona Snow. Vypadá dosti zabezpečená.", "sperhak");
        Chest kralovskaTruhla = new Chest("kralovskaTruhla", "Královská truhla. Vypadá dosti zabezpečená.", "sperhak");

        // Přidáme předměty do truhly
        truhlaJonaSnow.addToChest("mec", mec);
        kralovskaTruhla.addToChest("vino", vino);
        kralovskaTruhla.addToChest("chleb", chleb);        

        // Vytvoříme jednotlivé lokace
        Area meereen = new Area("meereen","Meereen je otrokářské město, kde žijí nejbohatší obyvatelé východních zemí,\ntaktéž tu začíná naše cesta.");
        Area dorne = new Area("dorne", "Oblast Dorne je sídlem rodu Martellů. Leží v nejjižnějším cípu Západozemí a má pouštní podnebí.\nV písku je vidět cosi blyštivého.");
        Area kingsLanding = new Area("kingsLanding","Centrum Západozemí Královo přístaviště.\nZde stojí v hradě s názvem Rudá bašta Železný trůn,\nna který touží usednout několik rodů, aby ovládli Západozemí.\nToto místo by stálo za porozhlédnutí.");
        Area riverrun = new Area("riverrun","Řekotočí, sídlo rodu Tullyů je prakticky nedobytelné.\nNachází se totiž přímo ve vodě, která dokáže zaplavit celé okolí.\nVypadá to, že hrad je právě zaplavený, a času na koukání není mnoho.");
        Area ironIslands = new Area("ironIslands","Železné ostrovy, tato oblast se rozkládá na sedmi malých ostrovech u západního pobřeží Západozemí.\nNevypadá to tu bezpečně, rychle se porozhlédnout a pryč.");
        Area winterFell = new Area("winterFell","Zimohrad. Rodem, který obývá nejsevernější civilizovanou část Západozemí, jsou Starkové\na vypadá to, že tu pro tebe Jon Snow možná něco zanechal.");
        Area theWall = new Area("theWall","Zeď je místem, kde se odděluje Západozemí od necivilizovaného Severu.\nZeď byla prolomena a čeká tu na tebe obávaný Noční král, který chce ovládnout celou zemi.");

        // Nastavíme průchody mezi lokacemi (sousední lokace)
        meereen.addExit(dorne);

        dorne.addExit(meereen);
        dorne.addExit(kingsLanding);
        dorne.addExit(riverrun);

        kingsLanding.addExit(dorne);
        kingsLanding.addExit(riverrun);

        riverrun.addExit(dorne);
        riverrun.addExit(kingsLanding);
        riverrun.addExit(ironIslands);
        riverrun.addExit(winterFell);

        ironIslands.addExit(riverrun);
        ironIslands.addExit(winterFell);

        winterFell.addExit(riverrun);
        winterFell.addExit(ironIslands);
        winterFell.addExit(theWall);

        theWall.addExit(winterFell);

        // Přidáme předměty do lokací
        meereen.addItem(mesec);
        dorne.addItem(pisek);
        dorne.addItem(sperhak);
        kingsLanding.addItem(zeleznyTrun);
        riverrun.addItem(lod);
        riverrun.addItem(padlo);
        ironIslands.addItem(sperhak);
        ironIslands.addItem(socha);

        // Přidáme truhly do lokací
        winterFell.addChest(truhlaJonaSnow);
        kingsLanding.addChest(kralovskaTruhla);

        // Vytvoříme osoby/nepřátele
        Enemy daenerys = new Enemy("daenerys", "Já jsem Daenerys Targaryen, vítej, tu máš mešec zlata na své cesty.");
        Enemy tyrion = new Enemy("tyrion", "Já jsem Tyrion Lannister, ztratil jsem tu špěrhák, když ho najdeš, nech si ho, mohl by se hodit.");
        Enemy cersei = new Enemy("cersei", "Já jsem Cersei Lannister, vítej v Králově přístavišti a moc tu neslídi.");
        Enemy jonSnow = new Enemy("jonSnow", "Já jsem Jon Snow a nechal jsem ti na WinterFellu meč, který budeš potřebovat..");
        Enemy nightKing = new Enemy("nightKing", "Já jsem Noční král, vůdce bílých chodců a tvůj nepřítel v cestě k vítězství.", true);

        // Přidáme osoby/nepřátele do lokací
        meereen.addEnemy(daenerys);
        dorne.addEnemy(tyrion);
        kingsLanding.addEnemy(cersei);
        winterFell.addEnemy(jonSnow);
        theWall.addEnemy(nightKing);

        // Hru začneme ve městě Meereen
        currentArea = meereen;
    }

    /**
     * Metoda vrací odkaz na aktuální lokaci, ve které se hráč právě nachází.
     *
     * @return aktuální lokace
     */
    public Area getCurrentArea() {
        return currentArea;
    }

    /**
     * Metoda nastaví aktuální lokaci, používá ji příkaz {@link CommandMove}
     * při přechodu mezi lokacemi.
     *
     * @param area lokace, která bude nastavena jako aktuální
     */
    public void setCurrentArea(Area area) {
        currentArea = area;
    }

    /**
     * Metoda vrací stav globální proměnné, sledující zabití posledního nepřítele a následnou výhru.
     * @return nepřitel byl zabit (true/false)
     */
    public boolean isVictorious() {
        return isWin;
    }

    /**
     * Metoda vrací stav globální proměnné, sledující zabití posledního nepřítele a následnou prohru.
     * @return nepřitel byl zabit (true/false)
     */
    public boolean isDefeated() {
        return isLose;
    }
}
